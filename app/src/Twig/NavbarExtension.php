<?php

namespace App\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class NavbarExtension extends AbstractExtension
{
    private Environment $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFunctions(): array
    {
        return [
                new TwigFunction('navbar', [$this, 'navbar'], ['needs_environment' => true, 'is_safe' => ['html']]),
        ];
    }

    public function navbar()
    {
        return $this->environment->render('layout/navbar.html.twig');
    }
}
