<?php

namespace App\Twig;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FooterExtension extends AbstractExtension
{
    private Environment $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('footer', [$this, 'footer'], ['needs_environment' => true, 'is_safe' => ['html']]),
        ];
    }

    public function footer()
    {
        return $this->environment->render('layout/footer.html.twig', [
            'fullName' => 'Faiez Rabboudi',
            'currentYear' => date("Y")
        ]);
    }
}
